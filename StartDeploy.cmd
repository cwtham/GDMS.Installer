@echo off
REM /*
REM /* GDMS Deployment batch script version 2018.06.05
REM /* Copyright (c) 2013 by Cw Tham (cw.tham@hyundai.com.my)
REM /*
REM /* 2016-02-03: Update file just for Git update to test AutoCRLF.
REM /* 2017-03-07: Replaced SAP Crystal 13 with version XI for Cash Collection module.
REM /* 2017-03-08: Add back SAP Crystal Reports Runtime Engine v13, required for RO/Invoice printing.
REM /* 2018-03-28: Add vehicle brand (FOR, HYU) check when executing batch. Exit if not specified as %1.
REM /* 2018-04-04: Add install overrides for single selection installation.
REM /*			   CLEANUP: Rearrange routine code execution sequence.
REM /* 2018-04-06: Beautify missing files info display.
REM /*			   Return to menu instead of exiting script.
REM /* 2018-04-07: Request for UAC if execution has no Admin privilege.
REM /* 2018-06-05: GDMSPRD profile injected for every users in registry (indiscrimately local/domain).
REM /*			   Prompt user for Brands and Web Client types.
REM /*
setlocal enableextensions enabledelayedexpansion
REM /* StartDeploy.cmd <make> <webclient> <debug>
set ver=2018.06.05
set debug=0
set make=%1
REM /* GDMS telnet server IP.
set gdmsip=172.17.3.10
goto :menu

REM /*** checkBrand routine: checks vehicle brand - HYU, FOR
:checkBrand
	IF "%make%" EQU "FOR" goto :eof
	IF "%make%" EQU "HYU" goto :eof
	IF "%make%" EQU "" (
		echo     ========================================================================
		echo     ^| Vehicle Brand is not specified. Do not execute this file on its own. ^|
		echo     ^|   Execute files 'Install.###.cmd' where ### = vehicle brand code.    ^|
		echo     ^|                                                                      ^|
		echo     ^|                       Press [Enter] to close.                        ^|
		echo     ========================================================================
		pause >nul
		exit /b 1
	)
	REM /* Vehicle Brand not catered for insallation.
	echo     ========================================================================
	echo     ^|              Vehicle Brand specified is not identified.              ^|
	echo     ^|   Execute files 'Install.###.cmd' where ### = vehicle brand code.    ^|
	echo     ^|                                                                      ^|
	echo     ^|                       Press [Enter] to close.                        ^|
	echo     ========================================================================
	pause >nul
	exit /b 1
REM /*** checkBrand routine

REM /*** prepVar routine: prepares variables - run once
:prepVar
	title Preparing variables . . .
	REM /* prepare in silence
	for /f %%a in ('copy /Z "%~dpf0" nul') do set "ASCII_13=%%a"
	for /f %%A in ('"prompt $H&for %%B in (1) do rem"') do set "BS=%%A"
	set NLM=^


	set NL=^^^%NLM%%NLM%^%NLM%%NLM%
	REM /* set WebClient default to FD/HD Live (dealer)
	IF "%webc%" EQU "" (
		IF "%make%" EQU "FOR" (
			set webc=FD
			set wclive=fdlive.prowcapc
			set wctrain=fdtrain.prowcapc
		)
		IF "%make%" EQU "HYU" (
			set webc=HD
			set wclive=hdlive.prowcapc
			set wctrain=hdtrain.prowcapc
		)
		set vpnuser=hd_preferences.xml
		set vpnglob=hd_preferences_global.xml
	)
	exit /b 0
REM /*** prepVar routine

REM /*** checkHome routine: make sure home folder is valid, otherwise variables would cause problem
:checkHome
	echo # Home folder       : "%~dp0"
	pushd "%~dp0" >nul 2>&1
	IF "%ERRORLEVEL%" EQU "1" (
		echo     ========================================================================
		echo     ^|    Unable to return to home folder. Please ensure folder does not    ^|
		echo     ^| does not contain symbols. Only alphanumeric characters are accepted. ^|
		echo     ^|                                                                      ^|
		echo     ^|                       Press [Enter] to close.                        ^|
		echo     ========================================================================
		pause >nul
	)
	exit /b %ERRORLEVEL%
REM /*** checkHome routine

REM /*** checkVersion routine: checks and displays Windows version
:checkVersion
	<nul set /p "=%BS% # Checking Windows version . . ."
	ver | findstr /i "5\.0\." >nul
	IF %ERRORLEVEL% EQU 0 (set osv=Win2k&& set cmdv=1&& set atype=1)
	ver | findstr /i "5\.1\." >nul
	IF %ERRORLEVEL% EQU 0 (set osv=WinXP&& set cmdv=1&& set atype=1)
	ver | findstr /i "5\.2\." >nul
	IF %ERRORLEVEL% EQU 0 (set osv=Win2k3&& set cmdv=1&& set atype=1)
	ver | findstr /i "6\.0\." >nul
	IF %ERRORLEVEL% EQU 0 (set osv=Vista&& set cmdv=2&& set atype=2)
	ver | findstr /i "6\.1\." >nul
	IF %ERRORLEVEL% EQU 0 (set osv=Win7&& set cmdv=2&& set atype=2)
	ver | findstr /i "6\.2\." >nul
	IF %ERRORLEVEL% EQU 0 (set osv=Win8&& set cmdv=2&& set atype=1)
	ver | findstr /i "6\.3\." >nul
	IF %ERRORLEVEL% EQU 0 (set osv=Win8&& set cmdv=2&& set atype=1)
	ver | findstr /i "10\.0\." >nul
	IF %ERRORLEVEL% EQU 0 (set osv=Win10&& set cmdv=2&& set atype=1)
	REM /* clear previous line
	<nul set /p "=%BS%!ASCII_13!                                                                        "
	<nul set /p "=%BS%!ASCII_13!# Windows version   : "
	ver | find /i "version"
	exit /b 0
REM /*** checkVersion routine

REM /*** checkArchitecture routine: checks Windows architecture - x86, x64
:checkArchitecture
	<nul set /p "=%BS% # Checking Processor Architecture . . ."
	REM /* Detects processor architecture -- x86/x64
	IF Defined ProgramFiles(x86) (
		REM /* x64
		set arch=x64
	) ELSE (
		REM /* x86
		set arch=x86
	)
	REM /* clear previous line
	<nul set /p "=%BS%!ASCII_13!                                                                        "
	echo !ASCII_13!# Processor arch.   : %arch%
	exit /b 0
REM /*** checkArchitecture routine

REM /*** checkPrivilege routine: checks if running user has Admin rights
:checkPrivilege
	<nul set /p "=%BS% # Checking Administrator Privileges . . ."
	set IsAdmin=0
	REM /* Type 1: Windows 2000, XP, 2003, Win8 privilege checks
	IF "%atype%" EQU "1" (
		REM /* Windows 2000, XP, 2003 command prompt scripting
		net session >nul 2>&1
		IF "!ERRORLEVEL!" EQU "0" (
			set IsAdmin=1
		) ELSE (
			echo.
			echo     ========================================================================
			echo     ^|           Requires Administrator privileges to install.              ^|
			echo     ^|                                                                      ^|
			echo     ^|           1^) Try 'runas' command or login as Administrator.          ^|
			echo     ^|           2^) or right-click -^> Run as administrator.                 ^|
			echo     ^|                                                                      ^|
			echo     ^|                       Press [Enter] to close.                        ^|
			echo     ========================================================================
			pause >nul
			exit /b 1
		)
	)
	REM /* Type 2: Windows Vista, Win7 privilege checks
	IF "%atype%" EQU "2" (
		REM /* Windows Vista, 7 command prompt scripting
		whoami /groups | findstr /b /c:"Mandatory Label\High Mandatory Level" | findstr /c:"Enabled group" >nul: && set IsAdmin=1
		IF "!IsAdmin!" EQU "0" (
			echo.
			echo     ========================================================================
			echo     ^|             Requires Administrator privileges to install.            ^|
			echo     ^|                                                                      ^|
			echo     ^|           You may try right-click -^> Run as administrator.           ^|
			echo     ^|                                                                      ^|
			echo     ^|                       Press [Enter] to close.                        ^|
			echo     ========================================================================
			pause >nul
			exit /b 1
		)
	)
	REM /* clear previous line
	<nul set /p "=%BS%!ASCII_13!                                                                        "
	echo !ASCII_13!# Admin privilege   : OK
exit /b 0
REM /*** checkPrivilege routine

REM /*** checkAdminPrivilege routine: checks if running user has Admin rights
:checkAdminPrivilege
	<nul set /p "=%BS% # Checking Administrator Privileges . . ."
	REM /* Checks for permission
	IF "%PROCESSOR_ARCHITECTURE%" EQU "amd64" (
		>nul 2>&1 "%SYSTEMROOT%\SysWOW64\cacls.exe" "%SYSTEMROOT%\SysWOW64\config\system"
	) ELSE (
		>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
	)
	REM /* If error flag set, we do not have admin.
	IF "%ERRORLEVEL%" NEQ "0" (
		echo.
		echo     ========================================================================
		echo     ^|             Requires Administrator privileges to install.            ^|
		echo     ^|                                                                      ^|
		echo     ^|   You may proceed with installation by requesting Admin privilege.   ^|
		echo     ========================================================================
		REM set /p getuac="# Request Admin privilege? [Y/N]: "
		REM IF /i "!getuac!" EQU "Y" ( call :requestUAC %make% )
		call :requestUAC %make%
		exit /b 1
	)
	REM /* clear previous line
	<nul set /p "=%BS%!ASCII_13!                                                                        "
	echo !ASCII_13!# Admin privilege   : OK
	exit /b 0
REM /*** checkAdminPrivilege routine

:requestUAC
	echo.
	echo Running installer with administrative privileges . . .
	echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
	set params=%*
	echo UAC.ShellExecute "cmd.exe", "/c ""%~s0"" %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
	"%temp%\getadmin.vbs"
	del "%temp%\getadmin.vbs"
	exit /b 0

REM /*** prepVar2 routine: prepares 2nd stage variables after all checks
:prepVar2
	IF "%cmdv%" EQU "1" (
		REM /* Windows 2000, XP, 2003
		set startup=%ALLUSERSPROFILE%\Start Menu\Programs\Startup
		set desktop=%ALLUSERSPROFILE%\Desktop
		set gdms=%ALLUSERSPROFILE%\Desktop\GDMS
		set appdatauser=%USERPROFILE%\Local Settings\Application Data
		set appdataall=%ALLUSERSPROFILE%\Application Data
		set startmenu=%ALLUSERSPROFILE%\Start Menu\Programs
	)
	IF "%cmdv%" EQU "2" (
		REM /* Windows Vista, 7, 8, 8.1, 10
		set startup=%ALLUSERSPROFILE%\Microsoft\Windows\Start Menu\Programs\Startup
		set desktop=%PUBLIC%\Desktop
		set gdms=%PUBLIC%\Desktop\GDMS
		set appdatauser=%LOCALAPPDATA%
		set appdataall=%ALLUSERSPROFILE%\
		set startmenu=%ALLUSERSPROFILE%\Microsoft\Windows\Start Menu\Programs
	)
	REM /* All GDMS related applications are 32-bit, hence
	REM /* define to x86 Program Files folder
	IF "%arch%" EQU "x86" (
		set progfiles=%ProgramFiles%
		set regkey=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run
		set wc9iss=%~dp010_Prerequisites\WC9_x86.iss
		set wc10iss=%~dp010_Prerequisites\WC10_x86.iss
	)
	IF "%arch%" EQU "x64" (
		set progfiles=!ProgramFiles^(x86^)!
		set regkey=HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Run
		set wc9iss=%~dp010_Prerequisites\WC9_x64.iss
		set wc10iss=%~dp010_Prerequisites\WC10_x64.iss
	)
	IF NOT EXIST "%gdms%" (md "%gdms%")
	exit /b 0
REM /*** prepVar2 routine

REM /*** checkDependancy routine: check file dependencies for each modules.
:checkDependancy
	<nul set /p "=%BS% # Checking Prerequisite Files . . ."
	call :validateFiles 1
	call :validateFiles 2
	call :validateFiles 3
	call :validateFiles 4
	call :validateFiles 5
	call :validateFiles 6
	call :validateFiles 7
	call :validateFiles 8
	<nul set /p "=%BS%!ASCII_13!                                                                        "
	IF "%i1stat%%i2stat%%i3stat%%i4stat%%i5stat%%i6stat%%i7stat%%i8stat%" EQU "OKOKOKOKOKOKOKOK" (
		echo !ASCII_13!# Prerequisites     : OK
	) ELSE (
		echo !ASCII_13!# Prerequisites     : Incomplete^^!
	)
	exit /b 0
REM /*** checkDependancy routine.

REM /*** showDebug routine: display debugging info. Flag enable/disable above.
:showDebug
	REM /* unremark these for variables debugging
	echo  ==================== [ Debug Info ] ====================
	echo    Version          : %ver%
	echo    StartUp          : %StartUp%
	echo    Desktop          : %Desktop%
	echo    GDMS             : %GDMS%
	echo    AppDataUser      : %AppDataUser%
	echo    AppDataAll       : %AppDataAll%
	echo    StartMenu        : %StartMenu%
	echo    Program Files    : %ProgFiles%
	echo    VPN user config  : %VPNUser%
	echo    VPN Global config: %VPNGlob%
	echo    FTP autorun key  : %RegKey% (deprecated)
	echo    WC9 Script       : %WC9ISS%
	echo    WC10 Script      : %WC10ISS%
	echo    Make / Webclient : %make% / %webc%
	echo    WC Live Prowcapc : %WCLive%
	echo    WC Train Prowcapc: %WCTrain%
	echo  ==================== [ End Info ] ====================
	echo.
	exit /b 0
REM /*** showDebug routine

:menu
title Performing Preliminary Checks . . .
call :checkBrand || goto :end
call :prepVar || goto :end
call :checkHome || goto :end
call :checkVersion || goto :end
call :checkArchitecture || goto :end
call :checkAdminPrivilege || goto :end
call :prepVar2 || goto :end
call :checkDependancy || goto :end
REM /* display debugging info
IF "%debug%" EQU "1" ( call :showDebug || goto :end )
REM /* reset menu flag to avoid executing last selection
REM /* when pressing [Enter] without input
set menu=
REM /* reset error flag
set errflag=0

title OS: [%osv%], Architecture: [%arch%] Main menu [v%ver%]
REM /* Installation menu (shared)
echo.
call :printLineExt
call :printBorder "      GDMS Deployment menu - [%make%]"
call :printLineExt
echo.
echo         [1^|a] - VPN client                                     ... [%i1stat%]
echo         [2^|b] - FTP server                                     ... [%i2stat%]
echo         [3^|c] - PuTTY                                          ... [%i3stat%]
echo         [4^|d] - Crystal Report                                 ... [%i4stat%]
echo         [5^|e] - Web Client v9                                  ... [%i5stat%]
echo         [6^|f] - Web Client v10                                 ... [%i6stat%]
echo         [7^|g] - %webc% Live/Train (requires Web Client v9) [s]     ... [%i7stat%]
echo         [8^|h] - Print Service (requires Web Client v10)        ... [%i8stat%]
echo.
echo           [x] - Exit
echo.
echo         [1-8] - install specified item and the rest of item(s).
echo         [a-h] - install ONLY single item.
REM echo           [s] - switch between HD Live/Train or HT Live/Train
echo.
set /p menu="# Please enter your choice: "
IF /i "%menu%" EQU "1" (
	set SINGLE_INSTALL=0
	goto 01_VPN
)
IF /i "%menu%" EQU "a" (
	set SINGLE_INSTALL=1
	goto 01_VPN
)
IF /i "%menu%" EQU "2" (
	set SINGLE_INSTALL=0
	goto 02_FTP
)
IF /i "%menu%" EQU "b" (
	set SINGLE_INSTALL=1
	goto 02_FTP
)
IF /i "%menu%" EQU "3" (
	set SINGLE_INSTALL=0
	goto 03_PUT
)
IF /i "%menu%" EQU "c" (
	set SINGLE_INSTALL=1
	goto 03_PUT
)
IF /i "%menu%" EQU "4" (
	set SINGLE_INSTALL=0
	goto 04_CRV
)
IF /i "%menu%" EQU "d" (
	set SINGLE_INSTALL=1
	goto 04_CRV
)
IF /i "%menu%" EQU "5" (
	set SINGLE_INSTALL=0
	goto 05_WC9
)
IF /i "%menu%" EQU "e" (
	set SINGLE_INSTALL=1
	goto 05_WC9
)
IF /i "%menu%" EQU "6" (
	set SINGLE_INSTALL=0
	goto 06_WC10
)
IF /i "%menu%" EQU "f" (
	set SINGLE_INSTALL=1
	goto 06_WC10
)
IF /i "%menu%" EQU "7" (
	set SINGLE_INSTALL=0
	goto 07_WCA
)
IF /i "%menu%" EQU "g" (
	set SINGLE_INSTALL=1
	goto 07_WCA
)
IF /i "%menu%" EQU "8" (
	set SINGLE_INSTALL=0
	goto 08_PRS
)
IF /i "%menu%" EQU "h" (
	set SINGLE_INSTALL=1
	goto 08_PRS
)
IF /i "%menu%" EQU "s" (
	IF /i "%make%" EQU "FOR" (
		IF /i "%webc%" EQU "FD" (
			set webc=FC
			set vpnuser=ht_preferences.xml
			set vpnglob=ht_preferences_global.xml
			set wclive=fclive.prowcapc
			set wctrain=fctrain.prowcapc
		)
		IF /i "%webc%" EQU "FC" (
			set webc=FD
			set vpnuser=hd_preferences.xml
			set vpnglob=hd_preferences_global.xml
			set wclive=fdlive.prowcapc
			set wctrain=fdtrain.prowcapc
		)
	)
	IF /i "%make%" EQU "HYU" (
		IF /i "%webc%" EQU "HD" (
			set webc=HT
			set vpnuser=ht_preferences.xml
			set vpnglob=ht_preferences_global.xml
			set wclive=htlive.prowcapc
			set wctrain=httrain.prowcapc
		)
		IF /i "%webc%" EQU "HT" (
			set webc=HD
			set vpnuser=hd_preferences.xml
			set vpnglob=hd_preferences_global.xml
			set wclive=hdlive.prowcapc
			set wctrain=hdtrain.prowcapc
		)
	)
	echo.
	echo                    ==========================================
	echo                    ^|            Switched to [!webc!]            ^|
	echo                    ==========================================
	echo.
	call :halt 2
	goto :menu
)
IF /i "%menu%" EQU "x" ( goto :end )
IF /i "%menu%" EQU "z" (
	call :validateFiles 1
	call :validateFiles 2
	call :validateFiles 3
	call :validateFiles 4
	call :validateFiles 5
	call :validateFiles 6
	call :validateFiles 7
	call :validateFiles 8
	
	REM /* hidden menu, show all missing files
	IF "%i1stat%%i2stat%%i3stat%%i4stat%%i5stat%%i6stat%%i7stat%%i8stat%" EQU "OKOKOKOKOKOKOKOK" (
		echo.
		echo               ====================================================
		echo               ^|     Prerequisites files verification passed^^!     ^|
		echo               ====================================================
		echo.
		call :halt 2
	) ELSE (
		echo.
		call :printLineExt
		call :printBorder "  Missing files:"
		call :printLineInt

		IF "%i1stat%" NEQ "OK" (
			call :printBorder "    VPN client:"
			call :validateFiles 1 Y
		)
		IF "%i2stat%" NEQ "OK" (
			call :printBorder "    FTP Server:"
			call :validateFiles 2 Y
		)
		IF "%i3stat%" NEQ "OK" (
			call :printBorder "    PuTTY:"
			call :validateFiles 3 Y
		)
		IF "%i4stat%" NEQ "OK" (
			call :printBorder "    Crystal Report:"
			call :validateFiles 4 Y
		)
		IF "%i5stat%" NEQ "OK" (
			call :printBorder "    WebClient v9:"
			call :validateFiles 5 Y
		)
		IF "%i6stat%" NEQ "OK" (
			call :printBorder "    WebClient v10:"
			call :validateFiles 6 Y
		)
		IF "%i7stat%" NEQ "OK" (
			call :printBorder "    WebClient App (Live/Train):"
			call :validateFiles 7 Y
		)
		IF "%i8stat%" NEQ "OK" (
			call :printBorder "    WebClient App (PrintService):"
			call :validateFiles 8 Y
		)
		call :printLineExt
		echo.
		echo Press [Enter] to continue . . .
		pause >nul
		echo.
	)
	goto :menu
)
REM /* when no valid selection is made
echo.
echo                    ==========================================
echo                    ^|           Invalid choice.              ^|
echo                    ==========================================
echo.
call :halt 2
goto :menu

REM /*** 01_VPN routine: installs Cisco AnyConnect.
:01_VPN
	title [%osv%] [%arch%] [v%ver%] Installing VPN client . . .
	REM /* Check failed - display missing files
	IF "%i1stat%" NEQ "OK" (
		echo.
		call :printLineExt
		call :printBorder "    Missing files for VPN setup:"
		call :printLineInt
		call :validateFiles 1 Y
		call :printLineExt
		echo.
		call :halt 2
		REM goto :menu
	) ELSE (
		echo     ========================================================================
		echo     ^|    01 VPN client setup . . .                                         ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /*   %ProgFiles%\Cisco\Cisco AnyConnect Secure Mobility Client\vpnui.exe
			echo     ^|       ^> Scanning existing installation . . .                         ^|
			IF EXIST "%ProgFiles%\Cisco\Cisco AnyConnect Secure Mobility Client\vpnui.exe" (
				REM /* vpnui.exe found, skip installation.
				echo     ^|         x VPN client already installed. Skipping installation.       ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       VPN client installation aborted.                               ^|
				echo     ========================================================================
			) ELSE (
				REM /* proceed with installation.
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				REM /* Prerequisite check: WinXP SP3 has been installed.
				IF "%osv%" EQU "WinXP" ( call :01_VPN_SP3 )
				call :01_VPN_ANYC
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			REM /* Prerequisite check: WinXP SP3 has been installed.
			IF "%osv%" EQU "WinXP" ( call :01_VPN_SP3 )
			call :01_VPN_ANYC
		)
		echo.
	)
	IF "%SINGLE_INSTALL%" EQU "0" (
		goto :02_FTP
	) ELSE (
		set SINGLE_INSTALL=
		call :halt 3
		goto :menu
	)

:01_VPN_SP3
	pushd 11_Big_Files
	echo     ^|                                                                      ^|
	echo     ^|       ^> Checking for Windows validity ^(Service Pack 3^) . . .         ^|
	reg query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion" /v CSDversion|Find "Service Pack 3">nul 2>&1&&(
		echo     ^|         + Operating System fulfilled requirements.                   ^|
	) || (
		echo     ^|         x Windows XP Service Pack 3 not installed.                   ^|
		echo     ^|         - Press [Enter] to proceed with installation.                ^|
		echo     ^|           ^(this will take a long while.^)                             ^|
		pause >nul
		IF EXIST "WindowsXP-KB936929-SP3-x86-ENU.exe" (
			REM /* use normal install to show progress
			echo     ^|         ^> Installing "WindowsXP-KB936929-SP3-x86-ENU.exe"            ^|
			call "WindowsXP-KB936929-SP3-x86-ENU.exe"
			IF "%ERRORLEVEL%" EQU "0" (
				echo     ^|           + Completed.                                               ^|
			) ELSE (
				echo     ^|           x Completed with error.                                    ^|
			)
		) ELSE (
			REM /* skip when file not found
			echo     ^|         x Skipped installation, file not found.                      ^|
		)
	)
	popd	%= pushd 11_Big_Files =%
	exit /b 0	%= 01_VPN_SP3 sub-routine =%

:01_VPN_ANYC
	pushd 01_VPN
	echo     ^|       ^> Installing "anyconnect-win-3.1.04059-web-deploy-k9.exe"      ^|
	call "anyconnect-win-3.1.04059-web-deploy-k9.exe" /qn
	IF "%ERRORLEVEL%" EQU "0" (
		pushd ..\10_Prerequisites
		echo     ^|         + Completed.                                                 ^|
		echo     ^|         + Configuring Cisco AnyConnect client ^(global^) . . .         ^|
		copy /v /y %vpnglob% "%AppDataAll%\Cisco\Cisco AnyConnect Secure Mobility Client\preferences_global.xml" >nul 2>&1
		echo     ^|         + Configuring Cisco AnyConnect client ^(user^) . . .           ^|
		copy /v /y %vpnuser% "%AppDataUser%\Cisco\Cisco AnyConnect Secure Mobility Client\preferences.xml" >nul 2>&1
		popd	%= pushd ..\10_Prerequisites =%
		echo     ^|         + Cleaning up configuration files . . .                      ^|
		IF EXIST "%ALLUSERSPROFILE%\Cisco\Cisco AnyConnect Secure Mobility Client\Profile\contractor_clientprofile.xml" (
			del /f /q "%ALLUSERSPROFILE%\Cisco\Cisco AnyConnect Secure Mobility Client\Profile\contractor_clientprofile.xml"
		)
		echo     ^|         + Creating shortcut file . . .                               ^|
		copy /v /y "%StartMenu%\Cisco\Cisco AnyConnect Secure Mobility Client\Cisco AnyConnect Secure Mobility Client.lnk" "%gdms%" >nul 2>&1

		IF %cmdv% EQU "2" (
			REM /* Fix DNS issue on Vista, 7, 8, 10
			echo     ^|                                                                      ^|
			echo     ^|       ^> Fixing DNS issue on Cisco AnyConnect Network Interface . . . ^|
			FOR /f "tokens=*" %%A IN ('reg query HKLM\SYSTEM\CurrentControlSet\Control\Class') DO (
				FOR /f "tokens=*" %%B IN ('reg query %%A') DO (
					reg query %%B /f "Cisco AnyConnect Secure Mobility Client" >nul 2>&1
					IF "!errorlevel!" EQU "0" (
						FOR /f "tokens=1-3" %%C IN ('reg query %%B /v NetCfgInstanceId ^| findstr /i NetCfgInstanceId') DO (
							reg query HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces\%%E /v RegistrationEnabled >nul 2>&1
							IF "!errorlevel!" EQU "0" (
								reg add HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces\%%E /v RegistrationEnabled /t REG_DWORD /d 0 /f >nul
								echo     ^|         + Done.                                                      ^|
							) else (
								echo     ^|         x Failed.                                                    ^|
							)
						)
					)
				)
			)
		)
		echo     ^|                                                                      ^|
		echo     ^|       VPN client installed and configured.                           ^|
		echo     ========================================================================
	) ELSE (
		REM /* anyconnect-win-3.1.04059-web-deploy-k9.exe installation error
		echo     ^|         x Error occurred during installation.                        ^|
		echo     ^|                                                                      ^|
		echo     ^|       VPN client installation aborted.                               ^|
		echo     ========================================================================
	)
	popd	%= pushd 01_VPN =%
	exit /b 0	%= 01_VPN_ANYC sub-routine =%

REM /*** :01_VPN routine.

REM /*** 02_FTP routine: installs FileZilla FTP Server.
:02_FTP
	title [%osv%] [%arch%] [v%ver%] Installing FTP server . . .
	REM /* Check failed - display missing files.
	IF "%i2stat%" NEQ "OK" (
		echo.
		call :printLineExt
		call :printBorder "    Missing files for FTP setup:"
		call :printLineInt
		call :validateFiles 2 Y
		call :printLineExt
		echo.
		call :halt 2
		REM goto :menu
	) ELSE (
		echo     ========================================================================
		echo     ^|    02 FTP server setup . . .                                         ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /*	%ProgFiles%\FileZilla Server\FileZilla Server.exe
			echo     ^|       ^> Scanning existing installation . . .                         ^|
			IF EXIST "%ProgFiles%\FileZilla Server\FileZilla Server.exe" (
				REM /* FileZilla Server.exe found, skip installation.
				echo     ^|         x FTP server already installed. Skipping installation.       ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       FTP server installation aborted.                               ^|
				echo     ========================================================================
			) ELSE (
				REM /* proceed with installation.
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :02_FTP_FZS
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			call :02_FTP_FZS
		)
		echo.
	)
	IF "%SINGLE_INSTALL%" EQU "0" (
		goto :03_PUT
	) ELSE (
		set SINGLE_INSTALL=
		call :halt 3
		goto :menu
	)

:02_FTP_FZS
	REM /* Latest FileZilla Server ver. that supports WinXP Service Mode.
	pushd 02_FTP
	echo     ^|       ^> Installing "FileZilla_Server-0_9_42.exe"                     ^|
	call "FileZilla_Server-0_9_42.exe" /S
	IF "%ERRORLEVEL%" EQU "0" (
		echo     ^|         + Completed.                                                 ^|
	) ELSE (
		echo     ^|         x Completed with error.                                      ^|
	)
	taskkill /f /im "FileZilla Server Interface.exe" >nul 2>&1
	pushd ..\10_Prerequisites
	echo     ^|         + Updating FileZilla configuration . . .                     ^|
	copy /v /y "FileZilla Server.xml" "%ProgFiles%\FileZilla Server\" >nul 2>&1
	popd	%= pushd ..\10_Prerequisites =%
	REM /* determine whether FileZilla Server.exe could be found, before doing /reload-config
	IF EXIST "%ProgFiles%\FileZilla Server\FileZilla Server.exe" (
		call "%ProgFiles%\FileZilla Server\FileZilla Server.exe" /reload-config
		echo     ^|           - Done.                                                    ^|
		echo     ^|         + Removing autorun registry entry . . .                      ^|
		reg delete "%regkey%" /v "FileZilla Server Interface" /f >nul 2>&1
		echo     ^|         + Creating 'ftproot' and 'tmp' folders . . .                 ^|
		md C:\ftproot >nul 2>&1
		md C:\tmp >nul 2>&1
		echo     ^|         + Creating shortcut file . . .                               ^|
		pushd ..\10_Prerequisites
		copy /v /y "[ftproot] @ C.lnk" "%gdms%" >nul 2>&1
		copy /v /y "[tmp] @ C.lnk" "%gdms%" >nul 2>&1
		popd	%= pushd ..\10_Prerequisites =%
		echo     ^|         + Deleting Desktop shortcut file . . .                       ^|
		del "%desktop%\FileZilla Server Interface.lnk" >nul 2>&1
		IF "%cmdv%" EQU "1" (
			REM /* Windows 2000, XP, 2003
			echo     ^|         + Fixing files and folders permissions . . .                 ^|
			cacls "C:\ftproot" /e /g Users:f >nul 2>&1
			cacls "C:\tmp" /e /g Users:f >nul 2>&1
			cacls "%gdms%\[ftproot] @ C.lnk" /e /g Users:f >nul 2>&1
			cacls "%gdms%\[tmp] @ C.lnk" /e /g Users:f >nul 2>&1
			echo     ^|         + Adding Firewall Rule for FileZilla . . .                   ^|
			REM netsh advfirewall firewall add rule name="FTP Server port 21" dir=in action=allow protocol=TCP localport=21 >nul 2>&1
			REM /* Delete rule first to avoid duplicated entries
			netsh firewall delete allowedprogram "%ProgFiles%\FileZilla Server\FileZilla Server.exe" >nul 2>&1
			netsh firewall add allowedprogram "%ProgFiles%\FileZilla Server\FileZilla Server.exe" "FileZilla FTP Server" ENABLE >nul 2>&1
		)
		IF "%cmdv%" EQU "2" (
			REM /* Windows Vista, 7, 8, 8.1, 10
			echo     ^|         + Fixing files and folders permissions . . .                 ^|
			icacls "C:\ftproot" /grant Users:^(NP^) >nul 2>&1
			icacls "C:\ftproot" /grant Users:F >nul 2>&1
			icacls "C:\tmp" /grant Users:^(NP^) >nul 2>&1
			icacls "C:\tmp" /grant Users:F >nul 2>&1
			icacls "%gdms%\[ftproot] @ C.lnk" /grant Users:F >nul 2>&1
			icacls "%gdms%\[tmp] @ C.lnk" /grant Users:F >nul 2>&1
			echo     ^|         + Adding Firewall Rule for FileZilla . . .                   ^|
			REM /* Delete rule first to avoid duplicated entries
			netsh advfirewall firewall delete rule name="FileZilla FTP Server %arch%" > nul 2>&1
			netsh advfirewall firewall add rule name="FileZilla FTP Server %arch%" dir=in action=allow program="%ProgFiles%\FileZilla Server\FileZilla Server.exe" enable=yes >nul 2>&1
		)
		echo     ^|                                                                      ^|
		echo     ^|       FTP server installed and configured.                           ^|
		echo     ========================================================================
	) ELSE (
		echo     ^|           x Failed.                                                  ^|
		echo     ^|                                                                      ^|
		echo     ^|       FTP server installation aborted.                               ^|
		echo     ========================================================================
	)
	popd	%= pushd 02_FTP =%
	exit /b 0	%= 02_FTP_FZS sub-routine =%

REM /* :02_FTP routine.

REM /*** 03_PUT routine: installs PuTTY telnet client.
:03_PUT
	title [%osv%] [%arch%] [v%ver%] Installing PuTTY . . .
	REM /* Check failed - display missing files.
	IF "%i3stat%" NEQ "OK" (
		echo.
		call :printLineExt
		call :printBorder "    Missing files for PuTTY setup:"
		call :printLineInt
		call :validateFiles 3 Y
		call :printLineExt
		echo.
		call :halt 2
		REM goto :menu
	) ELSE (
		echo     ========================================================================
		echo     ^|    03 PuTTY telnet client setup . . .                                ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /*	C:\PuTTY\putty.exe
			echo     ^|       ^> Scanning existing installation . . .                         ^|
			IF EXIST "C:\PuTTY\putty.exe" (
				REM /* putty.exe found, skip installation.
				echo     ^|         x PuTTY client already installed. Skipping installation.     ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       PuTTY telnet client installation aborted.                      ^|
				echo     ========================================================================
			) ELSE (
				REM /* proceed with installation.
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :03_PUT_CPY
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			call :03_PUT_CPY
		)
		echo.
	)
	IF "%SINGLE_INSTALL%" EQU "0" (
		goto :04_CRV
	) ELSE (
		set SINGLE_INSTALL=
		call :halt 3
		goto :menu
	)

:03_PUT_CPY
	pushd 03_PUT
	echo     ^|       ^> Copying PuTTY files                                          ^|
	xcopy /s /r /y putty.exe C:\PuTTY\ >nul 2>&1
	pushd ..\10_Prerequisites
	echo     ^|         + Creating shortcut file . . .                               ^|
	copy /v /y "GDMS.lnk" "%gdms%\" >nul 2>&1
	REM /* GDMS standard profile
	REM echo     ^|         + Creating [GDMSPRD] profile . . .                           ^|
	REM call "GDMSPRD_profile.cmd" >nul
	call :03_PUT_PRO
	IF "%cmdv%" EQU "1" (
		REM /* Windows 2000, XP, 2003 - Courier New
		REM echo     ^|         + Applying Windows GUI settings . . .                        ^|
		REM call "GDMSPRD_profile_WinGUI.cmd" >nul
		call :03_PUT_WinGUI
		echo     ^|         + Fixing files and folders permissions . . .                 ^|
		cacls "C:\PuTTY" /e /g Users:f >nul 2>&1
		cacls "%gdms%\GDMS.lnk" /e /g Users:f >nul 2>&1
	)
	IF "%cmdv%" EQU "2" (
		IF "%osv%" EQU "Vista" (
			REM /* Windows Vista - Courier New
			REM echo     ^|         + Applying Windows GUI settings . . .                        ^|
			REM call "GDMSPRD_profile_WinGUI.cmd" >nul
			call :03_PUT_WinGUI
		) ELSE (
			REM /* Windows 7, 8, 8.1, 10 - Consolas
			REM echo     ^|         + Applying Windows Aero GUI settings . . .                   ^|
			REM call "GDMSPRD_profile_WinAero.cmd" >nul
			call :03_PUT_WinAero
		)
		echo     ^|         + Fixing files and folders permissions . . .                 ^|
		icacls "C:\PuTTY" /grant Users:^(NP^) >nul 2>&1
		icacls "C:\PuTTY" /grant Users:F >nul 2>&1
		icacls "%gdms%\GDMS.lnk" /grant Users:F >nul 2>&1
	)
	popd	%= pushd ..\10_Prerequisites =%
	echo     ^|                                                                      ^|
	echo     ^|       PuTTY telnet client installed and configured.                  ^|
	echo     ========================================================================
	popd	%= pushd 03_PUT =%
	exit /b 0	%= 03_PUT_CPY sub-routine =%

:03_PUT_PRO
	set querykey=\Software\Microsoft\Windows\CurrentVersion\Run
	set writekey=\Software\SimonTatham\PuTTY\Sessions\GDMSPRD
	echo     ^|         + Creating [GDMSPRD] profile . . .                           ^|
	FOR /f "tokens=1-2 delims=\" %%A IN ('reg query HKU') DO (
		reg query %%A\%%B%subkey% >nul 2>&1
		IF "!ERRORLEVEL!" EQU "0" (
			set regkey=%%B
			set regkey=!regkey:~0,41!
			REM call :printBar "           > %%B"
			call :printBar "           > !regkey!"
			reg add "%%A\%%B%writekey%" /v "AddressFamily" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "AgentFwd" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "AltF4" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "AltOnly" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "AltSpace" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "AlwaysOnTop" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ANSIColour" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "Answerback" /t REG_SZ /d "PuTTY" /f >nul
			reg add "%%A\%%B%writekey%" /v "ApplicationCursorKeys" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ApplicationKeypad" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "AuthKI" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "AuthTIS" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "AutoWrapMode" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "BackspaceIsDelete" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BCE" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "Beep" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "BeepInd" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BellOverload" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "BellOverloadN" /t REG_DWORD /d "5" /f >nul
			reg add "%%A\%%B%writekey%" /v "BellOverloadS" /t REG_DWORD /d "5000" /f >nul
			reg add "%%A\%%B%writekey%" /v "BellOverloadT" /t REG_DWORD /d "2000" /f >nul
			reg add "%%A\%%B%writekey%" /v "BellWaveFile" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "BlinkCur" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BlinkText" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BoldAsColour" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "BoldFont" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "BoldFontCharSet" /t REG_DWORD /d "546683640" /f >nul
			reg add "%%A\%%B%writekey%" /v "BoldFontHeight" /t REG_DWORD /d "1999335925" /f >nul
			reg add "%%A\%%B%writekey%" /v "BoldFontIsBold" /t REG_DWORD /d "1638264" /f >nul
			reg add "%%A\%%B%writekey%" /v "BugDeriveKey2" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BugHMAC2" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BugIgnore1" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BugPKSessID2" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BugPlainPW1" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BugRSA1" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "BugRSAPad2" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "CapsLockCyr" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ChangeUsername" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Cipher" /t REG_SZ /d "aes,blowfish,3des,WARN,arcfour,des" /f >nul
			reg add "%%A\%%B%writekey%" /v "CJKAmbigWide" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "CloseOnExit" /t REG_DWORD /d "2" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour0" /t REG_SZ /d "0,0,0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour1" /t REG_SZ /d "255,255,255" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour10" /t REG_SZ /d "0,128,0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour11" /t REG_SZ /d "85,255,85" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour12" /t REG_SZ /d "187,187,0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour13" /t REG_SZ /d "255,255,85" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour14" /t REG_SZ /d "0,0,128" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour15" /t REG_SZ /d "85,85,255" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour16" /t REG_SZ /d "187,0,187" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour17" /t REG_SZ /d "255,85,255" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour18" /t REG_SZ /d "0,187,187" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour19" /t REG_SZ /d "85,255,255" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour2" /t REG_SZ /d "192,192,192" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour20" /t REG_SZ /d "187,187,187" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour21" /t REG_SZ /d "255,255,255" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour3" /t REG_SZ /d "85,85,85" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour4" /t REG_SZ /d "0,0,0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour5" /t REG_SZ /d "255,255,128" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour6" /t REG_SZ /d "0,0,0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour7" /t REG_SZ /d "85,85,85" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour8" /t REG_SZ /d "128,0,0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Colour9" /t REG_SZ /d "255,85,85" /f >nul
			reg add "%%A\%%B%writekey%" /v "ComposeKey" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Compression" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "CtrlAltKeys" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "CurType" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "DECOriginMode" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "DisableArabicShaping" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "DisableBidi" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Environment" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "EraseToScrollback" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "FontCharSet" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "FontHeight" /t REG_DWORD /d "10" /f >nul
			reg add "%%A\%%B%writekey%" /v "FontIsBold" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "FontVTMode" /t REG_DWORD /d "4" /f >nul
			reg add "%%A\%%B%writekey%" /v "FullScreenOnAltEnter" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "HideMousePtr" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "HostName" /t REG_SZ /d "172.17.3.10" /f >nul
			reg add "%%A\%%B%writekey%" /v "KEX" /t REG_SZ /d "dh-gex-sha1,dh-group14-sha1,dh-group1-sha1,WARN" /f >nul
			reg add "%%A\%%B%writekey%" /v "LFImpliesCR" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "LineCodePage" /t REG_SZ /d "CP437" /f >nul
			reg add "%%A\%%B%writekey%" /v "LinuxFunctionKeys" /t REG_DWORD /d "5" /f >nul
			reg add "%%A\%%B%writekey%" /v "LocalEcho" /t REG_DWORD /d "2" /f >nul
			reg add "%%A\%%B%writekey%" /v "LocalEdit" /t REG_DWORD /d "2" /f >nul
			reg add "%%A\%%B%writekey%" /v "LocalPortAcceptAll" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "LocalUserName" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "LockSize" /t REG_DWORD /d "2" /f >nul
			reg add "%%A\%%B%writekey%" /v "LogFileClash" /t REG_DWORD /d "4294967295" /f >nul
			reg add "%%A\%%B%writekey%" /v "LogFileName" /t REG_SZ /d "putty.log" /f >nul
			reg add "%%A\%%B%writekey%" /v "LogFlush" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "LoginShell" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "LogType" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "MouseIsXterm" /t REG_DWORD /d "2" /f >nul
			reg add "%%A\%%B%writekey%" /v "MouseOverride" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "NetHackKeypad" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoAltScreen" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoApplicationCursors" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoApplicationKeys" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoDBackspace" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoMouseReporting" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoPTY" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoRemoteCharset" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoRemoteQTitle" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoRemoteResize" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "NoRemoteWinTitle" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "PassiveTelnet" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "PasteRTF" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "PingInterval" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "PingIntervalSecs" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "PortForwardings" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "PortNumber" /t REG_DWORD /d "23" /f >nul
			reg add "%%A\%%B%writekey%" /v "Present" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "Printer" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "Protocol" /t REG_SZ /d "telnet" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyDNS" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyExcludeList" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyHost" /t REG_SZ /d "proxy" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyLocalhost" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyMethod" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyPassword" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyPort" /t REG_DWORD /d "80" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyTelnetCommand" /t REG_SZ /d "connect %host %port\n" /f >nul
			reg add "%%A\%%B%writekey%" /v "ProxyUsername" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "PublicKeyFile" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "RawCNP" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "RectSelect" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "RekeyBytes" /t REG_SZ /d "1G" /f >nul
			reg add "%%A\%%B%writekey%" /v "RekeyTime" /t REG_DWORD /d "60" /f >nul
			reg add "%%A\%%B%writekey%" /v "RemoteCommand" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "RemotePortAcceptAll" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "RFCEnviron" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "RXVTHomeEnd" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ScrollbackLines" /t REG_DWORD /d "200" /f >nul
			reg add "%%A\%%B%writekey%" /v "ScrollBar" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "ScrollBarFullScreen" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ScrollbarOnLeft" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ScrollOnDisp" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "ScrollOnKey" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ShadowBold" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "ShadowBoldOffset" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "SSH2DES" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "SSHLogOmitData" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "SSHLogOmitPasswords" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "SshNoAuth" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "SshNoShell" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "SshProt" /t REG_DWORD /d "2" /f >nul
			reg add "%%A\%%B%writekey%" /v "StampUtmp" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "SunkenEdge" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "TCPKeepalives" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "TCPNoDelay" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "TelnetKey" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "TelnetRet" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "TermHeight" /t REG_DWORD /d "25" /f >nul
			reg add "%%A\%%B%writekey%" /v "TerminalModes" /t REG_SZ /d "INTR=A,QUIT=A,ERASE=A,KILL=A,EOF=A,EOL=A,EOL2=A,START=A,STOP=A,SUSP=A,DSUSP=A,REPRINT=A,WERASE=A,LNEXT=A,FLUSH=A,SWTCH=A,STATUS=A,DISCARD=A,IGNPAR=A,PARMRK=A,INPCK=A,ISTRIP=A,INLCR=A,IGNCR=A,ICRNL=A,IUCLC=A,IXON=A,IXANY=A,IXOFF=A,IMAXBEL=A,ISIG=A,ICANON=A,XCASE=A,ECHO=A,ECHOE=A,ECHOK=A,ECHONL=A,NOFLSH=A,TOSTOP=A,IEXTEN=A,ECHOCTL=A,ECHOKE=A,PENDIN=A,OPOST=A,OLCUC=A,ONLCR=A,OCRNL=A,ONOCR=A,ONLRET=A,CS7=A,CS8=A,PARENB=A,PARODD=A," /f >nul
			reg add "%%A\%%B%writekey%" /v "TerminalSpeed" /t REG_SZ /d "38400,38400" /f >nul
			reg add "%%A\%%B%writekey%" /v "TerminalType" /t REG_SZ /d "xterm" /f >nul
			reg add "%%A\%%B%writekey%" /v "TermWidth" /t REG_DWORD /d "80" /f >nul
			reg add "%%A\%%B%writekey%" /v "TryPalette" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "UserName" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "UseSystemColours" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "UTF8Override" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "WarnOnClose" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "WideBoldFont" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "WideBoldFontCharSet" /t REG_DWORD /d "1635056" /f >nul
			reg add "%%A\%%B%writekey%" /v "WideBoldFontHeight" /t REG_DWORD /d "16813452" /f >nul
			reg add "%%A\%%B%writekey%" /v "WideBoldFontIsBold" /t REG_DWORD /d "1016" /f >nul
			reg add "%%A\%%B%writekey%" /v "WideFont" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "WideFontCharSet" /t REG_DWORD /d "1638264" /f >nul
			reg add "%%A\%%B%writekey%" /v "WideFontHeight" /t REG_DWORD /d "1634796" /f >nul
			reg add "%%A\%%B%writekey%" /v "WideFontIsBold" /t REG_DWORD /d "1635056" /f >nul
			reg add "%%A\%%B%writekey%" /v "WindowBorder" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "WinNameAlways" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "WinTitle" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "Wordness0" /t REG_SZ /d "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Wordness128" /t REG_SZ /d "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1" /f >nul
			reg add "%%A\%%B%writekey%" /v "Wordness160" /t REG_SZ /d "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1" /f >nul
			reg add "%%A\%%B%writekey%" /v "Wordness192" /t REG_SZ /d "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2" /f >nul
			reg add "%%A\%%B%writekey%" /v "Wordness224" /t REG_SZ /d "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2" /f >nul
			reg add "%%A\%%B%writekey%" /v "Wordness32" /t REG_SZ /d "0,1,2,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1" /f >nul
			reg add "%%A\%%B%writekey%" /v "Wordness64" /t REG_SZ /d "1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,2" /f >nul
			reg add "%%A\%%B%writekey%" /v "Wordness96" /t REG_SZ /d "1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1" /f >nul
			reg add "%%A\%%B%writekey%" /v "X11AuthType" /t REG_DWORD /d "1" /f >nul
			reg add "%%A\%%B%writekey%" /v "X11Display" /t REG_SZ /d "" /f >nul
			reg add "%%A\%%B%writekey%" /v "X11Forward" /t REG_DWORD /d "0" /f >nul
			reg add "%%A\%%B%writekey%" /v "Xterm256Colour" /t REG_DWORD /d "1" /f >nul
		)
	)
	exit /b 0	%= 03_PUT_PRO sub-routine =%

:03_PUT_WinGUI
	set querykey=\Software\Microsoft\Windows\CurrentVersion\Run
	set writekey=\Software\SimonTatham\PuTTY\Sessions\GDMSPRD
	echo     ^|         + Applying Windows GUI settings . . .                        ^|
	FOR /f "tokens=1-2 delims=\" %%A IN ('reg query HKU') DO (
		reg query %%A\%%B%subkey% >nul 2>&1
		IF "!ERRORLEVEL!" EQU "0" (
			set regkey=%%B
			set regkey=!regkey:~0,41!
			REM call :printBar "           > %%B"
			call :printBar "           > !regkey!"
			reg add "%%A\%%B%writekey%" /v "Font" /t REG_SZ /d "Courier New" /f >nul
		)
	)
	exit /b 0	%= 03_PUT_WinGUI sub-routine =%

:03_PUT_WinAero
	set querykey=\Software\Microsoft\Windows\CurrentVersion\Run
	set writekey=\Software\SimonTatham\PuTTY\Sessions\GDMSPRD
	echo     ^|         + Applying Windows Aero GUI settings . . .                   ^|
	FOR /f "tokens=1-2 delims=\" %%A IN ('reg query HKU') DO (
		reg query %%A\%%B%subkey% >nul 2>&1
		IF "!ERRORLEVEL!" EQU "0" (
			set regkey=%%B
			set regkey=!regkey:~0,41!
			REM call :printBar "           > %%B"
			call :printBar "           > !regkey!"
			reg add "%%A\%%B%writekey%" /v "Font" /t REG_SZ /d "Consolas" /f >nul
		)
	)
	exit /b 0	%= 03_PUT_WinAero sub-routine =%

REM /*** :03_PUT routine.

REM /*** 04_CRV routine: installs Crystal Report programs (2).
:04_CRV
	title [%osv%] [%arch%] [v%ver%] Installing Crystal Report . . .
	REM /* Check failed - display missing files.
	IF "%i4stat%" NEQ "OK" (
		echo.
		call :printLineExt
		call :printBorder "    Missing files for Crystal Viewer setup:"
		call :printLineInt
		call :validateFiles 4 Y
		call :printLineExt
		echo.
		call :halt 2
		REM goto :menu
	) ELSE (
		echo     ========================================================================
		echo     ^|    04 Crystal Runtime XI setup . . .                                 ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /*	%ProgFiles%\\Business Objects\BusinessObjects Enterprise 11.5\
			echo     ^|       ^> Scanning existing installation . . .                         ^|
			IF EXIST "%ProgFiles%\Business Objects\BusinessObjects Enterprise 11.5\" (
				REM /* \BusinessObjects Enterprise 11.5\ found, skip installation.
				echo     ^|         x Crystal Reports already installed. Skipping installation.  ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       Crystal Runtime XI installation aborted.                       ^|
				echo     ^|======================================================================^|
			) ELSE (
				REM /* proceed with installation.
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :04_CRV_BOE
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			call :04_CRV_BOE
		)
		REM echo.
		REM echo     ^|======================================================================^|
		echo     ^|    04 SAP Crystal Reports Runtime Engine v13 setup . . .             ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /*	%ProgFiles%\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Common\Crystal Reports 2011
			echo     ^|       ^> Scanning existing installation . . .                         ^|
			IF EXIST "%ProgFiles%\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Common\Crystal Reports 2011\" (
				REM /* \Crystal Reports for .NET Framework 4.0\ found, skip installation.
				echo     ^|         x Crystal Reports already installed. Skipping installation.  ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       SAP Crystal Reports Runtime Engine v13 installation aborted.   ^|
				echo     ========================================================================
			) ELSE (
				REM /* proceed with installation.
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :04_CRV_REN
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			call :04_CRV_REN
		)
		echo.
	)
	IF "%SINGLE_INSTALL%" EQU "0" (
		goto :05_WC9
	) ELSE (
		set SINGLE_INSTALL=
		call :halt 3
		goto :menu
	)

:04_CRV_BOE
	pushd 04_CRV
	IF "%cmdv%" EQU "1" (
		echo     ^|       ^> Checking DotNET Framework v3.5 installation . . .            ^|
		IF EXIST "%windir%\Microsoft.NET\Framework\v3.5" (
			echo     ^|         + DotNET Framework v3.5 already installed.                   ^|
		) ELSE (
			echo     ^|         x DotNET Framework v3.5 not found.                           ^|
			echo     ^|         - Press [Enter] to proceed with installation.                ^|
			echo     ^|           ^(this will take a moment.^)                                 ^|
			pause >nul
			IF EXIST ..\11_Big_Files (
				pushd ..\11_Big_Files
				IF EXIST "dotnetfx35.exe" (
					echo     ^|         ^> Installing "dotnetfx35.exe"                                ^|
					call "dotnetfx35.exe"
					IF "%ERRORLEVEL%" EQU "0" (
						echo     ^|           + Completed.                                               ^|
					) ELSE (
						echo     ^|           x Completed with error.                                    ^|
					)
				) ELSE (
					REM /* skip when file not found
					echo     ^|         x Skipped installation, "dotnetfx35.exe" not found.          ^|
				)
				popd	%= pushd ..\11_Big_Files =%
			) ELSE (
				echo     ^|         x Skipped installation, 11_Big_Files folder not found.       ^|
			)
		)
		echo     ^|                                                                      ^|
	)
	echo     ^|       ^> Installing "CrystalRunTime XI Service Pack 3.msi"            ^|
	REM call "CrystalRunTime XI Service Pack 3.msi" ALLUSERS=1 /norestart /quiet
	call "CrystalRunTime XI Service Pack 3.msi" ALLUSERS=1 /norestart /passive
	IF "%ERRORLEVEL%" EQU "0" (
		echo     ^|         + Completed.                                                 ^|
	) ELSE (
		echo     ^|         x Completed with error.                                      ^|
	)
	echo     ^|                                                                      ^|
	echo     ^|       Crystal Runtime XI installed.                                  ^|
	echo     ========================================================================
	popd	%= pushd 04_CRV =%
	exit /b 0	%= 04_CRV_BOE sub-routine =%

:04_CRV_REN
	pushd 04_CRV
	IF "%cmdv%" EQU "1" (
		echo     ^|       ^> Checking DotNET Framework v3.5 installation . . .            ^|
		IF EXIST "%windir%\Microsoft.NET\Framework\v3.5" (
			echo     ^|         + DotNET Framework v3.5 already installed.                   ^|
		) ELSE (
			echo     ^|         x DotNET Framework v3.5 not found.                           ^|
			echo     ^|         - Press [Enter] to proceed with installation.                ^|
			echo     ^|           ^(this will take a moment.^)                                 ^|
			pause >nul
			IF EXIST ..\11_Big_Files (
				pushd ..\11_Big_Files
				IF EXIST "dotnetfx35.exe" (
					echo     ^|         ^> Installing "dotnetfx35.exe"                                ^|
					call "dotnetfx35.exe"
					IF "%ERRORLEVEL%" EQU "0" (
						echo     ^|           + Completed.                                               ^|
					) ELSE (
						echo     ^|           x Completed with error.                                    ^|
					)
				) ELSE (
					REM /* skip when file not found
					echo     ^|         x Skipped installation, "dotnetfx35.exe" not found.          ^|
				)
				popd	%= pushd ..\11_Big_Files =%
			) ELSE (
				echo     ^|         x Skipped installation, 11_Big_Files folder not found.       ^|
			)
		)
		echo     ^|                                                                      ^|
	)
	echo     ^|       ^> Installing "Crystal Report Runtime v13 32-bit"               ^|
	REM call "CRRuntime_32bit_13_0_6.msi" /norestart /quiet
	call "CRRuntime_32bit_13_0_6.msi" /norestart /passive
	IF "%ERRORLEVEL%" EQU "0" (
		echo     ^|         + Completed.                                                 ^|
	) ELSE (
		echo     ^|         x Completed with error.                                      ^|
	)
	echo     ^|                                                                      ^|
	echo     ^|       SAP Crystal Reports Runtime Engine v13 installed.              ^|
	echo     ========================================================================
	popd	%= pushd 04_CRV =%
	exit /b 0	%= 04_CRV_REN sub-routine =%

REM /*** :04_CRV routine.

REM /*** 05_WC9 routine: installs Progress WebClient v9.
:05_WC9
	title [%osv%] [%arch%] [v%ver%] Installing Web Client v9 . . .
	REM /* Check failed - display missing files.
	IF "%i5stat%" NEQ "OK" (
		echo.
		call :printLineExt
		call :printBorder "    Missing files for WebClient v9 setup:"
		call :printLineInt
		call :validateFiles 5 Y
		call :printLineExt
		echo.
		call :halt 2
		REM goto :menu
	) ELSE (
		echo     ========================================================================
		echo     ^|    05 Progress Web Client v9 setup . . .                             ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /* 	%ProgFiles%\Progress Software\WebClient\bin\prowc.exe
			echo     ^|       ^> Scanning existing installation . . .                         ^|
			IF EXIST "%ProgFiles%\Progress Software\WebClient\bin\prowc.exe" (
				REM /* prowc.exe found, skip installation.
				echo     ^|         x Web Client v9 already installed. Skipping installation.    ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       Web Client v9 installation aborted.                            ^|
				echo     ========================================================================
			) ELSE (
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :05_WC9_SET
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			call :05_WC9_SET
		)
		echo.
	)
	IF "%SINGLE_INSTALL%" EQU "0" (
		goto :06_WC10
	) ELSE (
		set SINGLE_INSTALL=
		call :halt 3
		goto :menu
	)

:05_WC9_SET
	pushd 05_WC9
	echo     ^|       ^> Starting Web Client v9 installation . . .                    ^|
	call Setup.exe -s -f1"%wc9iss%"
	IF "%ERRORLEVEL%" EQU "0" (
		echo     ^|         + Completed.                                                 ^|
	) ELSE (
		echo     ^|         x Completed with error.                                      ^|
	)
	echo     ^|                                                                      ^|
	echo     ^|       Progress Web Client v9 installed.                              ^|
	echo     ========================================================================
	popd	%= pushd 05_WC9 =%
	exit /b 0	%= 05_WC9_SET sub-routine end =%

REM /* :05_WC9 routine.

REM /*** 06_WC10 routine: installs Progress WebClient v10.
:06_WC10
	title [%osv%] [%arch%] [v%ver%] Installing Web Client v10 . . .
	REM /* Check failed - display missing files.
	IF "%i6stat%" NEQ "OK" (
		echo.
		call :printLineExt
		call :printBorder "    Missing files for WebClient v10 setup:"
		call :printLineInt
		call :validateFiles 6 Y
		call :printLineExt
		echo.
		call :halt 2
		REM goto :menu
	) ELSE (
		echo     ========================================================================
		echo     ^|    06 Progress OpenEdge v10 setup . . .                              ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /*	"%ProgFiles%\Progress Software\WebClient10\bin\prowc.exe
			echo     ^|       ^> Scanning existing installation . . .                         ^|
			IF EXIST "%ProgFiles%\Progress Software\WebClient10\bin\prowc.exe" (
				REM /* prowc.exe found, skip installation.
				echo     ^|         x OpenEdge v10 already installed. Skipping installation.     ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       OpenEdge v10 installation aborted.                             ^|
				echo     ========================================================================
			) ELSE (
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :06_WC10_SET
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			call :06_WC10_SET
		)
		echo.
	)
	IF "%SINGLE_INSTALL%" EQU "0" (
		goto :07_WCA
	) ELSE (
		set SINGLE_INSTALL=
		call :halt 3
		goto :menu
	)

:06_WC10_SET
	pushd 06_WC10
	echo     ^|       ^> Starting Open Edge v10 installation . . .                    ^|
	call setup.exe -s -f1"%wc10iss%"
	IF "%ERRORLEVEL%" EQU "0" (
		echo     ^|         + Completed.                                                 ^|
	) ELSE (
		echo     ^|         x Completed with error.                                      ^|
	)
	REM /* add firewall whitelist entry here
	IF "%cmdv%" EQU "1" (
		echo     ^|         + Adding Firewall Port 2020 Inbound Rule . . .               ^|
		REM /* Delete rule first to avoid duplicated entries
		netsh firewall delete allowedprogram "%ProgFiles%\Progress Software\WebClient10\bin\prowc.exe" >nul 2>&1
		netsh firewall add allowedprogram "%ProgFiles%\Progress Software\WebClient10\bin\prowc.exe" "GDMS PrintService" ENABLE >nul 2>&1
	)
	IF "%cmdv%" EQU "2" (
		echo     ^|         + Adding Firewall Rule for GDMS PrintService . . .           ^|
		REM /* Delete rule first to avoid duplicated entries
		netsh advfirewall firewall delete rule name="GDMS PrintService" > nul 2>&1
		netsh advfirewall firewall add rule name="GDMS PrintService" dir=in action=allow program="%ProgFiles%\Progress Software\WebClient10\bin\prowc.exe" enable=yes >nul 2>&1
	)
	echo     ^|                                                                      ^|
	echo     ^|       Progress OpenEdge v10 installed.                               ^|
	echo     ========================================================================
	popd	%= pushd 06_WC10 =%
	exit /b 0	%= 06_WC10_SET sub-routine end =%

REM /*** :06_WC10 routine.

REM /*** 07_WCA routine: installs SD Web Client apps.
:07_WCA
	title [%osv%] [%arch%] [v%ver%] Configuring %webc% WebClient module . . .
	REM /* Check failed - display missing files.
	IF "%i7stat%" NEQ "OK" (
		echo.
		call :printLineExt
		call :printBorder "    Missing files for WebClient apps setup:"
		call :printLineInt
		call :validateFiles 7 Y
		call :printLineExt
		echo.
		call :halt 2
		REM goto :menu
	) ELSE (
		echo     ========================================================================
		echo     ^|    07 WebClient Live/Train applications setup . . .                  ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /*	%ProgFiles%\WebClientApps\SIME\%webc% Live GDMS\
			REM /*	Live: %webc%: FC, FD, HD, HT
			echo     ^|       ^> Scanning %webc% Live installation . . .                          ^|
			IF EXIST "%ProgFiles%\WebClientApps\SIME\%webc% Live GDMS\" (
				REM /* \%webc% Live GDMS\ found, skip installation
				echo     ^|         x Application found. Skipping installation.                  ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       WebClient %webc% Live installation aborted.                        ^|
				echo     ^|======================================================================^|
			) ELSE (
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :07_WCA_LIV
			)
			REM /*	%ProgFiles%\WebClientApps\SIME\%webc% Live GDMS\
			REM /*	Live: %webc%: FC, FD, HD, HT
			echo     ^|       ^> Scanning %webc% Train installation . . .                         ^|
			IF EXIST "%ProgFiles%\WebClientApps\SIME\%webc% Train GDMS\" (
				REM /* \%webc% Train GDMS\ found, skip installation
				echo     ^|         x Application found. Skipping installation.                  ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       WebClient %webc% Train installation aborted.                       ^|
				echo     ========================================================================
			) ELSE (
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :07_WCA_TRN
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			call :07_WCA_LIV
			call :07_WCA_TRN
		)
		echo.
	)
	IF "%SINGLE_INSTALL%" EQU "0" (
		goto :08_PRS
	) ELSE (
		set SINGLE_INSTALL=
		call :halt 3
		goto :menu
	)

:07_WCA_LIV
	call :07_WCA_checkVPN
	IF "!ERRORLEVEL!" EQU "1" (popd && goto :menu)
	pushd 07_WCA
	echo     ^|                                                                      ^|
	echo     ^|       ^> Installing %webc% Live client application . . .                  ^|
	call "%wclive%"
	echo     ^|         - Press [Enter] after closing login dialog.                  ^|
	pause >nul
	echo     ^|         + Setting folder permissions . . .                           ^|
	IF "%cmdv%" EQU "1" (
		REM /* Windows 2000, XP, 2003
		cacls "%ProFiles%\WebClientApps\SIME\%webc% Live GDMS" /e /g Users:f >nul 2>&1
	)
	IF "%cmdv%" EQU "2" (
		REM /* Windows Vista, 7, 8, 8.1, 10
		icacls "%ProFiles%\WebClientApps\SIME\%webc% Live GDMS" /grant Users:^(NP^) >nul 2>&1
		icacls "%ProFiles%\WebClientApps\SIME\%webc% Live GDMS" /grant Users:F >nul 2>&1
	)
	echo     ^|         + Moving shortcut file . . .                                 ^|
	move /y "%desktop%\%webc% Live GDMS.lnk" "%gdms%" >nul 2>&1
	echo     ^|                                                                      ^|
	echo     ^|       WebClient %webc% Live application installed.                       ^|
	echo     ^|======================================================================^|
	popd	%= pushd 07_WCA =%
	exit /b 0	%= 07_WCA_LIV sub-routine end =%

:07_WCA_TRN
	call :07_WCA_checkVPN
	IF "!ERRORLEVEL!" EQU "1" (popd && goto :menu)
	pushd 07_WCA
	echo     ^|                                                                      ^|
	echo     ^|       ^> Installing %webc% Train client application . . .                 ^|
	call "%wctrain%"
	echo     ^|         - Press [Enter] after closing login dialog.                  ^|
	pause >nul
	echo     ^|         + Setting folder permissions . . .                           ^|
	IF "%cmdv%" EQU "1" (
		REM /* Windows 2000, XP, 2003
		cacls "%ProFiles%\WebClientApps\SIME\%webc% Train GDMS" /e /g Users:f >nul 2>&1
	)
	IF "%cmdv%" EQU "2" (
		REM /* Windows Vista, 7, 8, 8.1, 10
		icacls "%ProFiles%\WebClientApps\SIME\%webc% Train GDMS" /grant Users:^(NP^) >nul 2>&1
		icacls "%ProFiles%\WebClientApps\SIME\%webc% Train GDMS" /grant Users:F >nul 2>&1
	)
	echo     ^|         + Moving shortcut file . . .                                 ^|
	move /y "%desktop%\%webc% Train GDMS.lnk" "%gdms%" >nul 2>&1
	echo     ^|                                                                      ^|
	echo     ^|       WebClient %webc% Train applications installed.                     ^|
	echo     ========================================================================
	popd	%= 07_WCA_TRN =%
	exit /b 0	%= 07_WCA_TRN sub-routine =%

:07_WCA_checkVPN
	REM /* subroutine: detect VPN connection, return errorlevel
	echo     ^|       ^> Detecting VPN connection to Wisma Sime Darby . . .           ^|
	ping -n 1 %gdmsip% | findstr /r /c:"[0-9] *ms" >nul
	IF "%ERRORLEVEL%" EQU "1" (
		REM /* Unable to reach server, request user to establish VPN connection
		echo     ^|         x VPN connection not established.                            ^|
		echo     ^|         - Please establish VPN using Cisco AnyConnect.               ^|
		REM /* execute Cisco AnyConnect on user's behalf
		start "" "%gdms%\Cisco AnyConnect Secure Mobility Client.lnk" >nul 2>&1
		echo     ^|         - Press [Enter] to continue after VPN is established.        ^|
		pause >nul
		ping -n 1 %gdmsip% | findstr /r /c:"[0-9] *ms" >nul
		IF "!ERRORLEVEL!" EQU "1" (
			REM /* something went wrong, VPN not established, or user naughty, exit
			echo     ^|         x VPN connection still not established. Exiting . . .        ^|
			echo     ========================================================================
			set ret=1
		) ELSE (
			echo     ^|         + VPN connection status: ACTIVE                              ^|
			set ret=0
		)
	) ELSE (
		echo     ^|         + VPN connection status: ACTIVE                              ^|
		set ret=0
	)
	exit /b %ret%

REM /*** 07_WCA routine.

REM /*** 08_PRS routine: installs SD PrintService.
:08_PRS
	title [%osv%] [%arch%] [v%ver%] Configuring Printing Service . . .
	REM /* Check failed - display missing files.
	IF "%i8stat%" NEQ "OK" (
		echo.
		call :printLineExt
		call :printBorder "    Missing files for PrintService setup:"
		call :printLineInt
		call :validateFiles 8 Y
		call :printLineExt
		echo.
		call :halt 2
		REM goto :menu
	) ELSE (
		echo     ========================================================================
		echo     ^|    08 GDMS PrintService application setup . . .                      ^|
		echo     ^|======================================================================^|
		IF "%SINGLE_INSTALL%" EQU "0" (
			REM /* Sequential installation. Check for existing installation:
			REM /*	%ProgFiles%\WebClientApps\SIME\PrintService\
			echo     ^|       ^> Scanning GDMS PrintService installation . . .                ^|
			IF EXIST "%ProgFiles%\WebClientApps\SIME\PrintService\rcode\printmain.r" (
				REM /* \PrintService\ found, skip installation.
				echo     ^|         x Application found. Skipping installation.                  ^|
				echo     ^|         - To proceed with installation, choose single install.       ^|
				echo     ^|                                                                      ^|
				echo     ^|       GDMS PrintService installation aborted.                        ^|
				echo     ========================================================================
			) ELSE (
				echo     ^|         + No existing installation found.                            ^|
				echo     ^|                                                                      ^|
				call :08_PRS_INS
			)
		) ELSE (
			REM /* Single installation. Skip existing installation check.
			call :08_PRS_INS
		)
		echo.
	)
	IF "%SINGLE_INSTALL%" EQU "0" (
		goto :menu
	) ELSE (
		set SINGLE_INSTALL=
		call :halt 3
		goto :menu
	)

:08_PRS_INS
	call :07_WCA_checkVPN
	IF "!ERRORLEVEL!" EQU "1" (popd && goto :menu)
	pushd 07_WCA
	echo     ^|                                                                      ^|
	echo     ^|       ^> Installing GDMS PrintService application . . .               ^|
	call "printservice.prowcapc"
	echo     ^|         - Press [Enter] after closing pop up dialog.                 ^|
	pause >nul
	echo     ^|         + Moving shortcut file . . .                                 ^|
	move /y "%desktop%\GDMS PrintService.lnk" "%gdms%" >nul 2>&1
	echo     ^|                                                                      ^|
	echo     ^|       GDMS PrintService application installed.                       ^|
	echo     ========================================================================
	popd	%= pushd 07_WCA =%
	exit /b 0	%= 08_PRS_INS sub-routine end =%

REM /*** 08_PRS routine.

REM /* globally available subroutines for cmd batch
:halt
IF "%1" EQU "" exit /b 1
ping -n %1 127.0.0.1 >nul
exit /b 0

:printLineExt
	echo  ==============================================================================
	exit /b 0

:printLineInt
	echo  ^|============================================================================^|
	exit /b 0

:printBar
    IF "%~1" EQU "" (
		echo     ^|                                                                      ^|
    ) ELSE (
        set "inText=%~1"
		<nul set /p "=%BS%     |                                                                      |"
		echo !ASCII_13!    ^|!inText!
    )
	exit /b 0	%= printBar sub-routine =%
REM /* printBar routine

REM /*** validateFiles routine: Flag OK/xx if files are complete/missing.
REM /***						Display listing of missing file.
REM /***	param: module no., display list
REM /***		   1 y
:validateFiles
	IF "%1" EQU "" ( exit /b 1 )
	IF "%1" EQU "1" (	%= VPN =%
		set i1stat=OK
		IF NOT EXIST "01_VPN\anyconnect-win-3.1.04059-web-deploy-k9.exe" ( 
			set i1stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [01_VPN\anyconnect-win-3.1.04059-web-deploy-k9.exe]                  ^|
			)
		)
		IF /i "%make%" EQU "FOR" (
			IF /i "%webc%" EQU "FD" (
				IF NOT EXIST "10_Prerequisites\hd_preferences.xml" (
					set i1stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\hd_preferences.xml]                                ^|
					)
				)
				IF NOT EXIST "10_Prerequisites\hd_preferences_global.xml" (
					set i1stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\hd_preferences_global.xml]                         ^|
					)
				)
			)
			IF /i "%webc%" EQU "FC" (
				IF NOT EXIST "10_Prerequisites\ht_preferences.xml" (
					set i1stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\ht_preferences.xml]                                ^|
					)
				)
				IF NOT EXIST "10_Prerequisites\ht_preferences_global.xml" (
					set i1stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\ht_preferences_global.xml]                         ^|
					)
				)
			)
		)
		IF /i "%make%" EQU "HYU" (
			IF /i "%webc%" EQU "HD" (
				IF NOT EXIST "10_Prerequisites\hd_preferences.xml" (
					set i1stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\hd_preferences.xml]                                ^|
					)
				)
				IF NOT EXIST "10_Prerequisites\hd_preferences_global.xml" (
					set i1stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\hd_preferences_global.xml]                         ^|
					)
				)
			)
			IF /i "%webc%" EQU "HT" (
				IF NOT EXIST "10_Prerequisites\ht_preferences.xml" (
					set i1stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\ht_preferences.xml]                                ^|
					)
				)
				IF NOT EXIST "10_Prerequisites\ht_preferences_global.xml" (
					set i1stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\ht_preferences_global.xml]                         ^|
					)
				)
			)
		)
	)
	IF "%1" EQU "2" (	%= FTP =%
		set i2stat=OK
		IF NOT EXIST "02_FTP\FileZilla_Server-0_9_42.exe" (
			set i2stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [02_FTP\FileZilla_Server-0_9_42.exe]                                 ^|
			)
		)
		IF NOT EXIST "10_Prerequisites\FileZilla Server.xml" (
			set i2stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [10_Prerequisites\FileZilla Server.xml]                              ^|
			)
		)
		IF NOT EXIST "10_Prerequisites\[ftproot] @ C.lnk" (
			set i2stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [10_Prerequisites\[ftproot] @ C.lnk]                                 ^|
			)
		)
		IF NOT EXIST "10_Prerequisites\[tmp] @ C.lnk" (
			set i2stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [10_Prerequisites\[tmp] @ C.lnk]                                     ^|
			)
		)
	)
	IF "%1" EQU "3" (
		%= PUT =%
		set i3stat=OK
		IF NOT EXIST "03_PUT\putty.exe" (
			set i3stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [03_PUT\putty.exe]                                                   ^|
			)
		)
		IF NOT EXIST "10_Prerequisites\GDMS.lnk" (
			set i3stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [10_Prerequisites\GDMS.lnk]                                          ^|
			)
		)
		IF /i "%cmdv%" EQU "1" (
			IF NOT EXIST "10_Prerequisites\GDMSPRD_profile_WinGUI.cmd" (
				set i3stat=xx
				IF /i "%2" EQU "Y" (
					echo  ^|       [10_Prerequisites\GDMSPRD_profile_WinGUI.cmd]                        ^|
				)	
			)
		) ELSE (
			IF /i "%osv%" EQU "Vista" (
				IF NOT EXIST "10_Prerequisites\GDMSPRD_profile_WinGUI.cmd" (
					set i3stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\GDMSPRD_profile_WinGUI.cmd]                        ^|
					)	
				)
			) ELSE (
				IF NOT EXIST "10_Prerequisites\GDMSPRD_profile_WinAero.cmd" (
					set i3stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [10_Prerequisites\GDMSPRD_profile_WinAero.cmd]                       ^|
					)	
				)
			)
		)
	)
	IF "%1" EQU "4" (	%= CRV =%
		set i4stat=OK
		IF NOT EXIST "04_CRV\CrystalRunTime XI Service Pack 3.msi" ( 
			set i4stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [04_CRV\CrystalRunTime XI Service Pack 3.msi]                        ^|
			)
		)
		IF NOT EXIST "04_CRV\CRRuntime_32bit_13_0_6.msi" ( 
			set i4stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [04_CRV\CRRuntime_32bit_13_0_6.msi]                                  ^|
			)
		)
	)
	IF "%1" EQU "5" (	%= WC9 =%
		set i5stat=OK
		IF NOT EXIST "05_WC9\data1.cab" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\data1.cab]                                                   ^|
			)
		)
		IF NOT EXIST "05_WC9\data1.hdr" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\data1.hdr]                                                   ^|
			)
		)
		IF NOT EXIST "05_WC9\data2.cab" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\data2.cab]                                                   ^|
			)
		)
		IF NOT EXIST "05_WC9\ikernel.ex_" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\ikernel.ex_]                                                 ^|
			)
		)
		IF NOT EXIST "05_WC9\ImgHiResIS6.bmp" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\ImgHiResIS6.bmp]                                             ^|
			)
		)
		IF NOT EXIST "05_WC9\ImgLoResIS6.bmp" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\ImgLoResIS6.bmp]                                             ^|
			)
		)
		IF NOT EXIST "05_WC9\layout.bin" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\layout.bin]                                                  ^|
			)
		)
		IF NOT EXIST "05_WC9\setup.bmp" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\setup.bmp]                                                   ^|
			)
		)
		IF NOT EXIST "05_WC9\Setup.exe" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\Setup.exe]                                                   ^|
			)
		)
		IF NOT EXIST "05_WC9\Setup.ini" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\Setup.ini]                                                   ^|
			)
		)
		IF NOT EXIST "05_WC9\setup.inx" ( 
			set i5stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [05_WC9\setup.inx]                                                   ^|
			)
		)
		IF /i "%arch%" EQU "x86" (
			IF NOT EXIST "10_Prerequisites\WC9_x86.iss" ( 
				set i5stat=xx
				IF /i "%2" EQU "Y" (
					echo  ^|       [10_Prerequisites\WC9_x86.iss]                                       ^|
				)
			)
		)
		IF /i "%arch%" EQU "x64" (
			IF NOT EXIST "10_Prerequisites\WC9_x64.iss" ( 
				set i5stat=xx
				IF /i "%2" EQU "Y" (
					echo  ^|       [10_Prerequisites\WC9_x64.iss]                                       ^|
				)
			)
		)
	)
	IF "%1" EQU "6" (	%= WC10 =%
		set i6stat=OK
		IF NOT EXIST "06_WC10\data1.cab" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\data1.cab]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\data1.hdr" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\data1.hdr]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\data2.cab" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\data2.cab]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\iis_wc.reg" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\iis_wc.reg]                                                 ^|
			)
		)
		IF NOT EXIST "06_WC10\ImgHiResIS6.bmp" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\ImgHiResIS6.bmp]                                            ^|
			)
		)
		IF NOT EXIST "06_WC10\ImgLoResIS6.bmp" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\ImgLoResIS6.bmp]                                            ^|
			)
		)
		IF NOT EXIST "06_WC10\ISSetup.dll" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\ISSetup.dll]                                                ^|
			)
		)
		IF NOT EXIST "06_WC10\lang.txt" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\lang.txt]                                                   ^|
			)
		)
		IF NOT EXIST "06_WC10\layout.bin" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\layout.bin]                                                 ^|
			)
		)
		IF NOT EXIST "06_WC10\setup.bmp" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\setup.bmp]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\setup.exe" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\setup.exe]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\setup.ini" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\setup.ini]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\setup.inx" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\setup.inx]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\setup.zip" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\setup.zip]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\setupapplet.jar" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\setupapplet.jar]                                            ^|
			)
		)
		IF NOT EXIST "06_WC10\_Graphics" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\_Graphics]                                                  ^|
			)
		)
		IF NOT EXIST "06_WC10\_Setup.dll" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [06_WC10\_Setup.dll]                                                 ^|
			)
		)
		IF /i "%arch%" EQU "x86" (
			IF NOT EXIST "10_Prerequisites\WC10_x86.iss" ( 
				set i6stat=xx
				IF /i "%2" EQU "Y" (
					echo  ^|       [10_Prerequisites\WC10_x86.iss]                                      ^|
				)
			)
		)
		IF /i "%arch%" EQU "x64" (
			IF NOT EXIST "10_Prerequisites\WC10_x64.iss" ( 
				set i6stat=xx
				IF /i "%2" EQU "Y" (
					echo  ^|       [10_Prerequisites\WC10_x64.iss]                                      ^|
				)
			)
		)
	)
	IF "%1" EQU "7" (	%= WCA =%
		set i7stat=OK
		IF /i "%make%" EQU "FOR" (
			IF /i "%webc%" EQU "FD" (
				IF NOT EXIST "07_WCA\fdlive.prowcapc" (
					set i7stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [07_WCA\fdlive.prowcapc]                                             ^|
					)
				)
				IF NOT EXIST "07_WCA\fdtrain.prowcapc" (
					set i7stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [07_WCA\fdtrain.prowcapc]                                            ^|
					)
				)
			)
			IF /i "%webc%" EQU "FC" (
				IF NOT EXIST "07_WCA\fclive.prowcapc" (
					set i7stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [07_WCA\fclive.prowcapc]                                             ^|
					)
				)
				IF NOT EXIST "07_WCA\fctrain.prowcapc" (
					set i7stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [07_WCA\fctrain.prowcapc]                                            ^|
					)
				)
			)
		)
		IF /i "%make%" EQU "HYU" (
			IF /i "%webc%" EQU "HD" (
				IF NOT EXIST "07_WCA\hdlive.prowcapc" (
					set i7stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [07_WCA\fctrain.prowcapc]                                            ^|
					)
				)
				IF NOT EXIST "07_WCA\hdtrain.prowcapc" (
					set i7stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [07_WCA\hdtrain.prowcapc]                                            ^|
					)
				)
			)
			IF /i "%webc%" EQU "HT" (
				IF NOT EXIST "07_WCA\htlive.prowcapc" (
					set i7stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [07_WCA\htlive.prowcapc]                                             ^|
					)
				)
				IF NOT EXIST "07_WCA\httrain.prowcapc" (
					set i7stat=xx
					IF /i "%2" EQU "Y" (
						echo  ^|       [07_WCA\httrain.prowcapc]                                            ^|
					)
				)
			)
		)
	)
	IF "%1" EQU "8" (	%= PRS =%
		set i8stat=OK
		IF NOT EXIST "07_WCA\printservice.prowcapc" ( 
			set i8stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [07_WCA\printservice.prowcapc]                                       ^|
			)
		)
	)
	exit /b 0

REM /*** validateFiles routine.

:validateWC10
	set i6stat=OK
	IF NOT EXIST "06_WC10\data1.cab" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\data1.cab]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\data1.hdr" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\data1.hdr]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\data2.cab" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\data2.cab]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\iis_wc.reg" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\iis_wc.reg]                                                 ^|
		)
	)
	IF NOT EXIST "06_WC10\ImgHiResIS6.bmp" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\ImgHiResIS6.bmp]                                            ^|
		)
	)
	IF NOT EXIST "06_WC10\ImgLoResIS6.bmp" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\ImgLoResIS6.bmp]                                            ^|
		)
	)
	IF NOT EXIST "06_WC10\ISSetup.dll" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\ISSetup.dll]                                                ^|
		)
	)
	IF NOT EXIST "06_WC10\lang.txt" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\lang.txt]                                                   ^|
		)
	)
	IF NOT EXIST "06_WC10\layout.bin" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\layout.bin]                                                 ^|
		)
	)
	IF NOT EXIST "06_WC10\setup.bmp" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\setup.bmp]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\setup.exe" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\setup.exe]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\setup.ini" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\setup.ini]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\setup.inx" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\setup.inx]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\setup.zip" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\setup.zip]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\setupapplet.jar" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\setupapplet.jar]                                            ^|
		)
	)
	IF NOT EXIST "06_WC10\_Graphics" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\_Graphics]                                                  ^|
		)
	)
	IF NOT EXIST "06_WC10\_Setup.dll" ( 
		set i6stat=xx
		IF /i "%2" EQU "Y" (
			echo  ^|       [06_WC10\_Setup.dll]                                                 ^|
		)
	)
	IF /i "%arch%" EQU "x86" (
		IF NOT EXIST "10_Prerequisites\WC10_x86.iss" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [10_Prerequisites\WC10_x86.iss]                                      ^|
			)
		)
	)
	IF /i "%arch%" EQU "x64" (
		IF NOT EXIST "10_Prerequisites\WC10_x64.iss" ( 
			set i6stat=xx
			IF /i "%2" EQU "Y" (
				echo  ^|       [10_Prerequisites\WC10_x64.iss]                                      ^|
			)
		)
	)
exit /b 0

REM /*** checkModule routine: checks files completion
:checkModule
	REM IF "%1" EQU "" goto :eof
	IF "%1" EQU "" ( exit /b 1 )
	REM /* checks VPN files
	IF "%1" EQU "1" (
		REM /* clear variables
		set i1stat=OK
		set i1mesg=
		call :checkFile i1stat i1mesg "01_VPN\anyconnect-win-3.1.04059-web-deploy-k9.exe"
		IF /i "%make%" EQU "FOR" (
			IF /i "%webc%" EQU "FD" (
				call :checkFile i1stat i1mesg "10_Prerequisites\hd_preferences.xml"
				call :checkFile i1stat i1mesg "10_Prerequisites\hd_preferences_global.xml"
			)
			IF /i "%webc%" EQU "FC" (
				call :checkFile i1stat i1mesg "10_Prerequisites\ht_preferences.xml"
				call :checkFile i1stat i1mesg "10_Prerequisites\ht_preferences_global.xml"
			)
		)
		IF /i "%make%" EQU "HYU" (
			IF /i "%webc%" EQU "HD" (
				call :checkFile i1stat i1mesg "10_Prerequisites\hd_preferences.xml"
				call :checkFile i1stat i1mesg "10_Prerequisites\hd_preferences_global.xml"
			)
			IF /i "%webc%" EQU "HT" (
				call :checkFile i1stat i1mesg "10_Prerequisites\ht_preferences.xml"
				call :checkFile i1stat i1mesg "10_Prerequisites\ht_preferences_global.xml"
			)
		)
	)
	REM /* checks FTP files
	IF "%1" EQU "2" (
		REM /* clear variables
		set i2stat=OK
		set i2mesg=
		call :checkFile i2stat i2mesg "02_FTP\FileZilla_Server-0_9_42.exe"
		call :checkFile i2stat i2mesg "10_Prerequisites\FileZilla Server.xml"
		call :checkFile i2stat i2mesg "10_Prerequisites\[ftproot] @ C.lnk"
		call :checkFile i2stat i2mesg "10_Prerequisites\[tmp] @ C.lnk"
	)
	REM /* checks PuTTY files
	IF "%1" EQU "3" (
		REM /* clear variables
		set i3stat=OK
		set i3mesg=
		call :checkFile i3stat i3mesg "03_PUT\putty.exe"
		call :checkFile i3stat i3mesg "10_Prerequisites\GDMS.lnk"
		IF /i "%cmdv%" EQU "1" (
			call :checkFile i3stat i3mesg "10_Prerequisites\GDMSPRD_profile_WinGUI.cmd"
		) ELSE (
			IF /i "%osv%" EQU "Vista" (
				call :checkFile i3stat i3mesg "10_Prerequisites\GDMSPRD_profile_WinGUI.cmd"
			) ELSE (
				call :checkFile i3stat i3mesg "10_Prerequisites\GDMSPRD_profile_WinAero.cmd"
			)
		)
	)
	REM /* checks SAP Crystal Viewer Runtime files
	IF "%1" EQU "4" (
		REM /* clear variables
		set i4stat=OK
		set i4mesg=
		call :checkFile i4stat i4mesg "04_CRV\CrystalRunTime XI Service Pack 3.msi"
		call :checkFile i4stat i4mesg "04_CRV\CRRuntime_32bit_13_0_6.msi"
	)
	REM /* checks Progress WebClient v9 files
	IF "%1" EQU "5" (
		REM /* clear variables
		set i5stat=OK
		set i5mesg=
		call :checkFile i5stat i5mesg "05_WC9\data1.cab"
		call :checkFile i5stat i5mesg "05_WC9\data1.hdr"
		call :checkFile i5stat i5mesg "05_WC9\data2.cab"
		call :checkFile i5stat i5mesg "05_WC9\ikernel.ex_"
		call :checkFile i5stat i5mesg "05_WC9\ImgHiResIS6.bmp"
		call :checkFile i5stat i5mesg "05_WC9\ImgLoResIS6.bmp"
		call :checkFile i5stat i5mesg "05_WC9\layout.bin"
		call :checkFile i5stat i5mesg "05_WC9\setup.bmp"
		call :checkFile i5stat i5mesg "05_WC9\Setup.exe"
		call :checkFile i5stat i5mesg "05_WC9\Setup.ini"
		call :checkFile i5stat i5mesg "05_WC9\setup.inx"
		IF /i "%arch%" EQU "x86" (
			call :checkFile i5stat i5mesg "10_Prerequisites\WC9_x86.iss"
		)
		IF /i "%arch%" EQU "x64" (
			call :checkFile i5stat i5mesg "10_Prerequisites\WC9_x64.iss"
		)
	)
	REM /* checks OpenEdge WebClient v10 files
	IF "%1" EQU "6" (
		REM /* clear variables
		set i6stat=OK
		set i6mesg=
		call :checkFile i6stat i6mesg "06_WC10\data1.cab"
		call :checkFile i6stat i6mesg "06_WC10\data1.hdr"
		call :checkFile i6stat i6mesg "06_WC10\data2.cab"
		call :checkFile i6stat i6mesg "06_WC10\iis_wc.reg"
		call :checkFile i6stat i6mesg "06_WC10\ImgHiResIS6.bmp"
		call :checkFile i6stat i6mesg "06_WC10\ImgLoResIS6.bmp"
		call :checkFile i6stat i6mesg "06_WC10\ISSetup.dll"
		call :checkFile i6stat i6mesg "06_WC10\lang.txt"
		call :checkFile i6stat i6mesg "06_WC10\layout.bin"
		call :checkFile i6stat i6mesg "06_WC10\setup.bmp"
		call :checkFile i6stat i6mesg "06_WC10\setup.exe"
		call :checkFile i6stat i6mesg "06_WC10\setup.ini"
		call :checkFile i6stat i6mesg "06_WC10\setup.inx"
		call :checkFile i6stat i6mesg "06_WC10\setup.zip"
		call :checkFile i6stat i6mesg "06_WC10\setupapplet.jar"
		call :checkFile i6stat i6mesg "06_WC10\_Graphics"
		call :checkFile i6stat i6mesg "06_WC10\_Setup.dll"
		IF /i "%arch%" EQU "x86" (
			call :checkFile i6stat i6mesg "10_Prerequisites\WC10_x86.iss"
		)
		IF /i "%arch%" EQU "x64" (
			call :checkFile i6stat i6mesg "10_Prerequisites\WC10_x64.iss"
		)
	)
	REM /* checks Webclient Apps: HD/HT Live/Train
	IF "%1" EQU "7" (
		REM /* clear variables
		set i7stat=OK
		set i7mesg=
		IF /i "%make%" EQU "FOR" (
			IF /i "%webc%" EQU "FD" (
				call :checkFile i7stat i7mesg "07_WCA\fdlive.prowcapc"
				call :checkFile i7stat i7mesg "07_WCA\fdtrain.prowcapc"
			)
			IF /i "%webc%" EQU "FC" (
				call :checkFile i7stat i7mesg "07_WCA\fclive.prowcapc"
				call :checkFile i7stat i7mesg "07_WCA\fctrain.prowcapc"
			)
		)
		IF /i "%make%" EQU "HYU" (
			IF /i "%webc%" EQU "HD" (
				call :checkFile i7stat i7mesg "07_WCA\hdlive.prowcapc"
				call :checkFile i7stat i7mesg "07_WCA\hdtrain.prowcapc"
			)
			IF /i "%webc%" EQU "HT" (
				call :checkFile i7stat i7mesg "07_WCA\htlive.prowcapc"
				call :checkFile i7stat i7mesg "07_WCA\httrain.prowcapc"
			)
		)
	)
	REM /* checks Webclient Apps: Print Service
	IF "%1" EQU "8" (
		REM /* clear variables
		set i8stat=OK
		set i8mesg=
		call :checkFile i8stat i8mesg "07_WCA\printservice.prowcapc"
	)
	REM /* clear previous line
	IF "%debug%" EQU "1" (
		<nul set /p "=%BS%!ASCII_13!                                                                        "
		echo !ASCII_13!   Done :checkModule %1.
	) ELSE (
		REM set chkModMsg=%chkModMsg% [%1:OK]
		<nul set /p "=%BS%!ASCII_13!                                                                        "
		<nul set /p "=%BS% %chkModMsg%"
	)
	exit /b 0
REM /*** checkModule routine.

REM /*** checkFile routine: param: status, message, filename
:checkFile
	IF NOT EXIST "%~3" (
		set "%1=xx"
		IF "!%2!" EQU "" (
			set "%2=!%2!        [%~3]"
		) ELSE (
			set "%2=!%2!!NL!        [%~3]"
		)
	)
	exit /b 0
REM /*** checkFile routine.

REM /* printBorder routine: parameter null or "string" (quoted)
:printBorder
	IF "%~1" EQU "" (
		echo  ^|                                                                            ^|
	) ELSE (
		set "inText=%~1"
		<nul set /p "=%BS%  |                                                                            |"
		echo !ASCII_13! ^|!inText!
	)
	exit /b 0	%= printBorder routine =%
REM /* printBorder routine

:end
echo.
echo                              ---==[ Goodbye! ]==---
echo.
call :halt 2
title %CMDCMDLINE:"=%
endlocal
